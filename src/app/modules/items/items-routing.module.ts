import { Routes } from '@angular/router';
import { RouterModule } from  '@angular/router';
import { SaladsComponent } from './salads/salads.component';
import { DessertsComponent } from './desserts/desserts.component';
import { BeveragesComponent } from './beverages/beverages.component';
import { BurgersComponent} from './burgers/burgers.component'
import { NgModule } from '@angular/core';
import { CartComponent } from './cart/cart.component';
import { FoodWrapperComponent } from './food-wrapper/food-wrapper.component';

const routes: Routes =[
    {
      path: 'burgers', component: BurgersComponent
    },
    {
      path: 'salads', component: SaladsComponent
    },
    {
      path: 'desserts', component: DessertsComponent
    },
    {
      path: 'beverages', component: BeveragesComponent
    },
    {
        path: 'cart', component: CartComponent
    },
    {
        path: 'food-wrapper', component: FoodWrapperComponent
    }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }


