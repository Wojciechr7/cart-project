import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import { FoodWrapperComponent } from './food-wrapper/food-wrapper.component';
import {ItemsRoutingModule} from './items-routing.module';
import { BurgersComponent } from './burgers/burgers.component';
import { BeveragesComponent } from './beverages/beverages.component';
import { SaladsComponent } from './salads/salads.component';
import { DessertsComponent } from './desserts/desserts.component';
import { CartComponent } from './cart/cart.component';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    ItemsRoutingModule,
    MatIconModule
  ],
  declarations: [
    BurgersComponent,
    BeveragesComponent,
    SaladsComponent,
    DessertsComponent,
    FoodWrapperComponent,
    CartComponent
  ]
})
export class ItemsModule { }
