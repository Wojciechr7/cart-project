import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public items = this.cartService.getItems();
  value: any;
  private totalAmount: number =0;
  total: number=0;
  constructor(
    private cartService: CartService
  ) { }
  ngOnInit(): void {
  this.total=0;
  }
  clearCart() {
    this.items = [];
    return this.items;
  }
  getTotalPrice(){
    this.total=0;
    this.items.map(item =>[
      this.total +=item.cost
    ]);
    return this.total;
  }
  minusone(){
    this.items.pop()
  }
}