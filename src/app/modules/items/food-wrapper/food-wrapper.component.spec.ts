import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodWrapperComponent } from './food-wrapper.component';

describe('FoodWrapperComponent', () => {
  let component: FoodWrapperComponent;
  let fixture: ComponentFixture<FoodWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
