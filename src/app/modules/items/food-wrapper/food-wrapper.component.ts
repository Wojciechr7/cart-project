import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Products} from '../products';
import { CartService } from '../cart.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-food-wrapper',
  templateUrl: './food-wrapper.component.html',
  styleUrls: ['./food-wrapper.component.scss']
})
export class FoodWrapperComponent implements OnInit {

  @Input() itemchoose!: string;
  items$: Observable<any> | undefined;
  constructor( private http: HttpClient,
    private route: ActivatedRoute,
    private cartService: CartService) { }
  
    addToCart(product: Products) {
      this.cartService.addToCart(product);
      window.alert('Your product has been added to the cart!');
    }
  ngOnInit(): void {
    console.log('itemchoose is',this.itemchoose);
    this.items$ = this.http.get('/api/'+this.itemchoose);
  }
}
