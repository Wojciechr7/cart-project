import { Injectable } from '@angular/core';
import { Products } from './products';
@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: Products[]= []
  total: number=0;
  
  constructor() {}
  addToCart(product: Products) {
    this.items.push(product);
  }

  getItems() {
    return this.items;
  }

  clearCart() {
    this.items = [];
    return this.items;
  }
  getTotalPrice(){
    this.items.map(item =>[
      this.total =+item.cost
    ]);
    return this.total;
  }
  minusone(product:Products){
    this.items.pop()
  }
}